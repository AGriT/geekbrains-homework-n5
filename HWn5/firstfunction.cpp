using namespace std;

extern "C" void vReverseArray(int** ppnReversedArray, int nArrSize)
{
	for (int i = 0; i < nArrSize; i++)
		ppnReversedArray[i] = (int*)!ppnReversedArray[i];
}