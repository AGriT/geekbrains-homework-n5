using namespace std;

extern "C" void vFillArray(int** ppnFilledArray, int nArrSize)
{
	for (int i = 0; i < nArrSize; i++)
		ppnFilledArray[i] = (int*)(i * 3 + 1);
}