#include <cstdlib>
using namespace std;

extern "C" void vMoveElements(int** ppnMovedArr, int nArrSize, int nMovedPos)
{
	if (nMovedPos < 0)
		nMovedPos += nArrSize;

	int* nMoveBuffer = (int*)calloc(nMovedPos, sizeof(int));
	int* nSavedBuffer = (int*)calloc(nMovedPos, sizeof(int));
		
	for (int i = nArrSize - nMovedPos; i < nArrSize; i++)
		nMoveBuffer[i - (nArrSize - nMovedPos)] = (int)ppnMovedArr[i];

	int i = 0;

	for (i = 0; i < nArrSize - nMovedPos; i += nMovedPos)
	{
		for (int j = 0; j < nMovedPos; j++)
		{
			nSavedBuffer[j] = (int)ppnMovedArr[i + j];
			ppnMovedArr[i + j] = (int*)nMoveBuffer[j];
		}

		int* nTempPtr = nSavedBuffer;
		nSavedBuffer = nMoveBuffer;
		nMoveBuffer = nTempPtr;
	}

	for (int j = 0; i < nArrSize; i++)
	{
		ppnMovedArr[i] = (int*)nMoveBuffer[j];
		j++;
	}
}