﻿#include <iostream>

using namespace std;

extern "C" void vReverseArray(int* pnFirstElement, int nCountOfElements);
extern "C" void vFillArray(int* pnArray, int nNumberOfElements);
extern "C" bool blCheckBalance(int* pnArray, int nNumberOfElements);
extern "C" void vMoveElements(int* ppnMovedArr, int nArrSize, int nMovedPos);

const int ncnstArrSize = 10;
const int ncnstArrFromP2 = 8;
const int ncnstThirdArray = 12;

int nArray[ncnstArrSize]{ 1, 0, 1, 0, 1, 0, 1, 0, 1, 1 };
int nSecondArray[ncnstArrFromP2]{};
int nThirdArray[ncnstThirdArray]{ 3, 4, 7, 8, 4, 4, 1, 6, 3, 5, 6, 1 };
int nFourthArray[ncnstArrSize]{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

int main()
{
	vReverseArray(&nArray[0], ncnstArrSize);
	for (int i = 0; i < ncnstArrSize; i++)
		cout << nArray[i] << ",";

	cout << "\n";
	//part 2

	vFillArray(&nSecondArray[0], ncnstArrFromP2);
	for (int i = 0; i < ncnstArrFromP2; i++)
		cout << nSecondArray[i] << ",";

	cout << "\n";

	//part 3
	if (blCheckBalance(&nThirdArray[0], ncnstThirdArray))
		cout << "True\n";
	else
		cout << "False\n";

	//part 4
	for (int i = 0; i < ncnstArrSize; i++)
		cout << nFourthArray[i] << ",";

	cout << "Moved to 3 elements\n";
	vMoveElements(&nFourthArray[0], ncnstArrSize, 3);

	for (int i = 0; i < ncnstArrSize; i++)
		cout << nFourthArray[i] << ",";
}
