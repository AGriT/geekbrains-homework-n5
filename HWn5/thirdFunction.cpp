using namespace std;

extern "C" bool blCheckBalance(int** nArray, int nNumberOfElements)
{
	int nLeftSumm = (int)nArray[0];
	int nRightSumm = 0;

	for (int i = 1; i < nNumberOfElements; i++)
		nRightSumm += (int)nArray[i];

	for (int i = 1; nLeftSumm <= nRightSumm; i++)
	{
		if (nLeftSumm == nRightSumm)
			return true;

		nLeftSumm += (int)nArray[i];
		nRightSumm -= (int)nArray[i];
	}

	return false;
}